import React from 'react';
import { StyleSheet, Text, View, Image, ScrollView } from 'react-native';

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.mainContainer}>
        <ScrollView
          horizontal
          contentContainerStyle={styles.storyContentContainer}
          style={styles.storyContainer}
          decelerationRate={0}
          snapToInterval={200} //your element width
          snapToAlignment={"center"}
        >

          <Text>Open up App.js to start working on your app!</Text>
          <Text>Open up App.js to start working on your app!</Text>
          <Text>Open up App.js to start working on your app!</Text>
          <Text>Open up App.js to start working on your app!</Text>
        </ScrollView>
        <ScrollView
          contentContainerStyle={styles.resourceContentContainer}
          style={styles.resourceContainer}
        >

          <Text>Open up App.js to start working on your app!</Text>
        </ScrollView>
        <ScrollView
          contentContainerStyle={styles.chatContentContainer}
          style={styles.chatContainer}
        >

          <Text>Open up App.js to start working on your app!</Text>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  storyContainer: {
    backgroundColor: '#fff',
    borderColor: 'red',
    borderStyle: 'solid',
    borderWidth: 1,
    flexGrow: 4
  },

  storyContentContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },

  resourceContainer: {
    backgroundColor: '#fff',
    borderColor: 'red',
    borderStyle: 'solid',
    borderWidth: 1,
    flexGrow: 2
  },
  resourceContentContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  chatContainer: {
    backgroundColor: '#fff',
    borderColor: 'red',
    borderStyle: 'solid',
    borderWidth: 1,
    flexGrow: 6
  },
  chatContentContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  imageStyle: {

  }
});
